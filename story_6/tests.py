from django.test import TestCase, Client
from django.http import HttpRequest

# views
from . import views

# Models
from .models import Kegiatan, Person

class GeneralTest(TestCase):
  def test_does_pipeline_work(self):
    self.assertEquals(1, 1)

class TestURLS(TestCase):
  # Testing urls
  def test_base_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

  def test_base_url_template(self):
    response = self.client.get('')
    self.assertTemplateUsed(response, 'story_6/index.html')

  def test_get_add_kegiatan(self):
    response = Client().get('/add-kegiatan/')
    self.assertEquals(response.status_code, 302)

  def test_post_add_kegiatan_without_data(self):
    response = Client().post('/add-kegiatan/')
    self.assertEquals(response.status_code, 400)

  def test_post_add_kegiatan_response_with_data(self):
    response = Client().post('/add-kegiatan/', data={
      'kegiatan_name': 'nama_kegiatan'
    })
    self.assertEquals(response.status_code, 302)

  def test_post_add_kegiatan_object_count_with_data(self):
    response = Client().post('/add-kegiatan/', data={
      'kegiatan_name': 'nama_kegiatan'
    })
    self.assertEquals(Kegiatan.objects.all().count(), 1)

  def test_get_add_peserta(self):
    response = Client().get('/add-peserta/1/')
    self.assertEquals(response.status_code, 400)

  def test_post_add_peserta_without_object(self):
    response = Client().post('/add-peserta/1/')
    self.assertEquals(response.status_code, 400)

  def test_post_add_peserta_with_object_without_data(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().post(f'/add-peserta/{kegiatan_baru.pk}/')
    self.assertEquals(response.status_code, 400)

  def test_post_add_peserta_response_with_object_with_data(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().post(f'/add-peserta/{kegiatan_baru.pk}/', data={
      'nama_peserta': 'suatu_nama'
    })
    self.assertEquals(response.status_code, 302)

  def test_post_add_peserta_object_count_with_object_with_data(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().post(f'/add-peserta/{kegiatan_baru.pk}/', data={
      'nama_peserta': 'suatu_nama'
    })
    self.assertEquals(kegiatan_baru.peserta.all().count(), 1)

  def test_get_daftar_peserta_with_nonexisting_object(self):
    response = Client().get('/daftar-peserta/1/')
    self.assertEquals(response.status_code, 400)

  def test_get_daftar_peserta_with_existing_object(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().get(f'/daftar-peserta/{kegiatan_baru.pk}/')
    self.assertEquals(response.status_code, 200)

  def test_get_daftar_peserta_with_existing_object_template(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().get(f'/daftar-peserta/{kegiatan_baru.pk}/')
    self.assertTemplateUsed(response, 'story_6/daftar-peserta.html')

  def test_delete_kegiatan_url_exists(self):
    response = Client().get('/delete-kegiatan/1/')
    self.assertEquals(response.status_code, 400)

  def test_delete_kegiatan_invalid_pk(self):
    response = Client().get('/delete-kegiatan/1/')
    self.assertEquals(response.status_code, 400)

  def test_delete_kegiatan_valid_pk_response(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().get(f'/delete-kegiatan/{kegiatan_baru.pk}/')
    self.assertEquals(response.status_code, 302)

  def test_delete_kegiatan_valid_pk_object_count(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    response = Client().get(f'/delete-kegiatan/{kegiatan_baru.pk}/')
    self.assertEquals(Kegiatan.objects.all().count(), 0)

class TestViews(TestCase):
  # Testing views
  def test_index_view_response(self):
    response = views.index(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_index_view_content(self):
    response = views.index(HttpRequest())
    self.assertIn('Tambah Kegiatan', response.content.decode('utf8'))

  def test_add_peserta_view_invalid_pk_response(self):
    request = HttpRequest()
    request.method = 'POST'
    response = views.add_peserta(request, 1)
    self.assertEquals(response.status_code, 400)

  def test_add_peserta_view_valid_pk_response(self):
    kegiatan_baru = Kegiatan(name='hello')
    kegiatan_baru.save()
    request = HttpRequest()
    request.method = 'POST'
    request.POST['nama_peserta'] = ''
    response = views.add_peserta(request, kegiatan_baru.pk)
    self.assertEquals(response.status_code, 302)

  def test_add_kegiatan_view_without_data_response(self):
    request = HttpRequest()
    request.method = 'POST'
    response = views.add_kegiatan(request)
    self.assertEquals(response.status_code, 400)

  def test_add_kegiatan_view_with_data_response(self):
    request = HttpRequest()
    request.method = 'POST'
    request.POST['kegiatan_name'] = 'hello'
    response = views.add_kegiatan(request)
    self.assertEquals(response.status_code, 302)

  def test_add_kegiatan_view_with_data_object_count(self):
    request = HttpRequest()
    request.method = 'POST'
    request.POST['kegiatan_name'] = 'hello'
    response = views.add_kegiatan(request)
    self.assertEquals(Kegiatan.objects.all().count(), 1)

  def test_daftar_peserta_view_invalid_pk_response(self):
    response = views.daftar_peserta(HttpRequest())
    self.assertEquals(response.status_code, 400)

  def test_daftar_peserta_view_valid_pk_response(self):
    kegiatan = Kegiatan(name='hello')
    kegiatan.save()
    response = views.daftar_peserta(HttpRequest(), kegiatan.pk)
    self.assertEquals(response.status_code, 200)

  def test_daftar_peserta_view_content(self):
    kegiatan = Kegiatan(name='hello')
    kegiatan.save()
    response = views.daftar_peserta(HttpRequest(), kegiatan.pk)
    self.assertIn('Daftar Peserta', response.content.decode('utf8'))

  def test_delete_kegiatan_view_invalid_pk_response(self):
    request = HttpRequest()
    self.assertEquals(views.delete_kegiatan(request, 1).status_code, 400)

  def test_delete_kegiatan_view_valid_pk_response(self):
    kegiatan = Kegiatan(name='hello')
    kegiatan.save()
    request = HttpRequest()
    self.assertEquals(views.delete_kegiatan(request, kegiatan.pk).status_code, 302)

  def test_delete_kegiatan_view_valid_pk_object_count(self):
    kegiatan = Kegiatan(name='hello')
    kegiatan.save()
    request = HttpRequest()
    views.delete_kegiatan(request, kegiatan.pk)
    self.assertEquals(Kegiatan.objects.all().count(), 0)

class TestModels(TestCase):
  # Testing models
  def test_create_kegiatan(self):
    kegiatan_baru = Kegiatan(name='nama_kegiatan')
    kegiatan_baru.save()
    self.assertEquals(Kegiatan.objects.all().count(), 1)

  def test_create_kegiatan_with_name(self):
    kegiatan_baru = Kegiatan(name='kegiatan_baru')
    kegiatan_baru.save()
    self.assertEquals(kegiatan_baru.name, 'kegiatan_baru')

  def test_create_person(self):
    kegiatan_baru = Kegiatan(name='kegiatan_baru')
    kegiatan_baru.save()
    person = Person(kegiatan=kegiatan_baru)
    person.save()
    self.assertEquals(Person.objects.all().count(), 1)

  def test_create_person_with_name(self):
    kegiatan_baru = Kegiatan(name='kegiatan_baru')
    kegiatan_baru.save()
    person = Person(name='person_name', kegiatan=kegiatan_baru)
    person.save()
    self.assertEquals(person.name, 'person_name')


