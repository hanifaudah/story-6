from django.urls import path, include
from .views import index, add_peserta, daftar_peserta, add_kegiatan, delete_kegiatan

urlpatterns = [
    path('', index, name='home'),
    path('add-kegiatan/', add_kegiatan, name='add-kegiatan'),
    path('delete-kegiatan/<int:pk>/', delete_kegiatan, name='delete-kegiatan'),
    path('add-peserta/<int:pk>/', add_peserta, name='add-peserta'),
    path('daftar-peserta/<int:pk>/', daftar_peserta, name='daftar-peserta'),
]