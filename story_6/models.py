from django.db import models

class Kegiatan(models.Model):
  name = models.CharField(max_length=128)

class Person(models.Model):
  kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='peserta')
  name = models.CharField(max_length=128)
